<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProyectoPublicado extends Model
{
    protected $table = 'proyectos_publicados';

    protected $fillable = [
    ];

    protected function getContracts($request)
    {
        $search = $request->get('search');
        return \App\ProyectoPublicado::search($search)->paginate('20');
    }

    public function scopeSearch($query, $search)
    {
        if(trim($search) != '') {
            $query->where("Nombre_del_proyecto", "LIKE", "%{$search}%");
        }
    }
}

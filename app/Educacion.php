<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Educacion extends Model
{

    protected $table = 'educacion_basica';

    protected $fillable = [
    ];

    protected function getPopulations($request)
    {
        $search = $request->get('search');
        return \App\Educacion::search($search)->paginate('20');
    }

    public function scopeSearch($query, $search)
    {
        if(trim($search) != '')
        {
            $query->where("DEPARTAMENTO", "LIKE", "%{$search}%")
            ->orderBy("AÑO","DESC");

        }
    }
}

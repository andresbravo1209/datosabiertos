<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Leyes extends Model
{
    protected $table = 'leyes_colombia';

    protected $fillable = [
    ];

    protected function getLaws($request)
    {
        $search = $request->get('search');
        return \App\Leyes::search($search)->paginate('20');
    }

    public function scopeSearch($query, $search)
    {
        if(trim($search) != '')
        {
            $query->where("NOMBRE", "LIKE", "%{$search}%");

        }
    }
}

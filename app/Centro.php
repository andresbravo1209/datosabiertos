<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Centro extends Model
{
    protected $table = 'centros_de_salud';

    protected $fillable = [
    ];

    protected function getCentro($request)
    {
        $search = $request->get('search');
        return \App\Centro::search($search)->paginate('20');
    }

    public function scopeSearch($query, $search)
    {
        if(trim($search) != '')
        {
            $query->where("Depto", "LIKE", "%{$search}%")
                ->orderBy("N°_Caso","DESC");

        }
    }
}

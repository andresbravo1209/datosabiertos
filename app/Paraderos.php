<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Paraderos extends Model
{

    protected $table = 'paraderos_sitp';

    protected $fillable = [
    ];

    protected function getStops($request)
    {
        $search = $request->get('search');
        return \App\Paraderos::search($search)->paginate('20');
    }

    public function scopeSearch($query, $search)
    {
        if(trim($search) != '')
        {
            $query->where("direccion_paradero", "LIKE", "%{$search}%")
                  ->orwhere("localidad_paradero", "LIKE", "%{$search}%")
                ->orwhere("via_paradero", "LIKE", "%{$search}%")
            ;

        }
    }
}

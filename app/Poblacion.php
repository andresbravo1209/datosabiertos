<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Poblacion extends Model
{
    protected $table = 'casos_positivos_de_covid_19_en_colombia';

    protected $fillable = [
    ];

    protected function getPoblacion ($request)
    {
        $search = $request->get('search');
        return \App\Poblacion::search($search)->paginate('20');
    }

    public function scopeSearch($query, $search)
    {
        if(trim($search) != '')
        {
            $query
            ->where("Ciudad", "LIKE", "%{$search}%")
            ->orWhere("Atencion", "LIKE", "%{$search}%")
            ->orWhere("Departamento_Distrito", "LIKE", "%{$search}%")
            ->orWhere("Edad", "LIKE", "%{$search}%")
            ->orWhere("Sexo", "LIKE", "%{$search}%")
            ->orWhere("Tipo", "LIKE", "%{$search}%")
            ->orWhere("Estado", "LIKE", "%{$search}%")
            ->orWhere("País_Procedencia", "LIKE", "%{$search}%");




}}}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GasolineraBogota extends Model
{
    protected $table = 'gasolineras_bogota';

    protected $fillable = [
    ];

    protected function getLocations($request)
    {
        $search = $request->get('search');
        return \App\GasolineraBogota::search($search)->paginate('20');
    }

    public function scopeSearch($query, $search)
    {
        if(trim($search) != '') {
            $query->where("NombreDepartamento", "LIKE", "%{$search}%")
                ->orWhere("direccion", "LIKE", "%{$search}%")
                ->orWhere("bandera", "LIKE", "%{$search}%");
        }
    }
}

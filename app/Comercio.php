<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comercio extends Model
{

    protected $table =  'camara_de_comercio';

    protected $fillable = [
    ];

    protected function getCommerce($request)
    {
        $search = $request->get('search');
        return \App\Comercio::search($search)->paginate('20');
    }

    public function scopeSearch($query, $search)
    {
        if(trim($search) != '')
        {
            $query->where("Nombre", "LIKE", "%{$search}%");

        }
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Empleados extends Model
{

    protected $table = 'fondo_empleados';

    protected $fillable = [
    ];

    protected function getEmployees($request)
    {
        $search = $request->get('search');
        return \App\Empleados::search($search)->paginate('20');
    }

    public function scopeSearch($query, $search)
    {
        if(trim($search) != '')
        {
            $query->where("nombreentidad", "LIKE", "%{$search}%");

        }
    }
}

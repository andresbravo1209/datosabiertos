<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CuadrantePolicial extends Model
{
    protected $table = 'contacto_cuadrante_policial';

    protected $fillable = [
    ];

    protected function getPolices($request)
    {
        $search = $request->get('search');
        return \App\CuadrantePolicial::search($search)->paginate('20');
    }

    public function scopeSearch($query, $search)
    {
        if(trim($search) != '') {
            $query->where("estacion", "LIKE", "%{$search}%");

        }
    }
}

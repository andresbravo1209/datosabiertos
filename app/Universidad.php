<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Universidad extends Model
{
    protected $table = 'universidades';

    protected $fillable = [
    ];

    protected function getUniversidad ($request)
    {
        $search = $request->get('search');

        return \App\Universidad::selectRaw('COUNT(DEPARTAMENTO) as cantidad, DEPARTAMENTO')->groupBy('DEPARTAMENTO')->get();


    }

    public function scopeSearch($query, $search)
    {
        if(trim($search) != '')
        {
            $query
            ->where("Nombre", "LIKE", "%{$search}%")
            ->orWhere("Direccion", "LIKE", "%{$search}%")
            ->orWhere("Telefono", "LIKE", "%{$search}%")
            ->orWhere("Localidad", "LIKE", "%{$search}%")
            ->orWhere("Domicilio", "LIKE", "%{$search}%")
            ->orWhere("Tel_Domicilio", "LIKE", "%{$search}%")
            ->orWhere("Pagina_Web", "LIKE", "%{$search}%");




}}}

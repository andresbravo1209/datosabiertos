<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Militar extends Model
{

    protected $table = 'distritos_militares';

    protected $fillable = [
    ];

    protected function getDistricts($request)
    {
        $search = $request->get('search');
        return \App\Militar::search($search)->paginate('20');
    }

    public function scopeSearch($query, $search)
    {
        if(trim($search) != '')
        {
            $query->where("CIUDAD", "LIKE", "%{$search}%")
            ->orderBy("ZONA","ASC");

        }
    }
}

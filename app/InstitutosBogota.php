<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InstitutosBogota extends Model
{
    protected $table = 'institutos_bogota';

    protected $fillable = [
    ];

    protected function getCompanies($request)
    {
        $search = $request->get('search');
        return \App\InstitutosBogota::search($search)->paginate('20');
    }

    public function scopeSearch($query, $search)
    {
        if(trim($search) != '')
        {
            $query->where("nombreinstitucion", "LIKE", "%{$search}%")
                ->orWhere("nitinstitucion","LIKE", "%{$search}")
                ->orWhere("codigoinstitucion","LIKE", "%{$search}");


        }
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ComercioBogota extends Model
{
    protected $table = 'comercio_bogota';

    protected $fillable = [
    ];

    protected function getComercioBta($request)
    {
        $search = $request->get('search');
        return \App\ComercioBogota::search($search)->paginate('20');
    }

    public function scopeSearch($query, $search)
    {
        if(trim($search) != '')
        {
            $query->where("actividad", "LIKE", "%{$search}%");

        }
    }
}

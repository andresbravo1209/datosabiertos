<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Covid extends Model
{
    protected $table = 'casos_positivos_de_covid_19_en_colombia';

    protected $fillable = [
    ];

    protected function getCovid($request)
    {
        $search = $request->get('search');
        return \App\Covid::search($search)->paginate('20');
    }

    public function scopeSearch($query, $search)
    {
        if(trim($search) != '')
        {
            $query->where("Depto", "LIKE", "%{$search}%")
                ->orderBy("N°_Caso","DESC");

        }
    }
}

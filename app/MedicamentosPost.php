<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MedicamentosPost extends Model
{
    protected $table = 'medicamentos';

    protected $fillable = [
    ];

    protected function getMedicines($request)
    {
        $search = $request->get('search');
        return \App\MedicamentosPost::search($search)->paginate('20');
    }

    public function scopeSearch($query, $search)
    {
        if(trim($search) != '')
        {
            $query->where("principio_activo", "LIKE", "%{$search}%") ;

        }
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Viviendas extends Model
{

    protected $table = 'vivienda_nueva';

    protected $fillable = [
    ];

    protected function getHomes($request)
    {
        $search = $request->get('search');
        return \App\Viviendas::search($search)->paginate('20');
    }

    public function scopeSearch($query, $search)
    {
        if(trim($search) != '')
        {
            $query->where("MUNICIPIO", "LIKE", "%{$search}%")
               ;

        }
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BibliotecasMedellin extends Model
{
    protected $table =  'bibliotecas_publicas_medellin';

    protected $fillable = [
    ];

    protected function getBibliotecasMedellin($request)
    {
        $search = $request->get('search');
        return \App\BibliotecasMedellin::search($search)->paginate('7');
    }

    public function scopeSearch($query, $search)
    {
        if(trim($search) != '') {
            $query->where("NOMBRE_BIBLIOTECA", "LIKE", "%{$search}%")
                ->orWhere("DIRECCIÓN", "LIKE", "%{$search}%")
                ->orWhere("TELEFONO", "LIKE", "%{$search}%")
                ->orWhere("TIPO", "LIKE", "%{$search}%")
                ->orWhere("COMUNA", "LIKE", "%{$search}%")
                ->orWhere("BARRIO", "LIKE", "%{$search}%")
                ->orWhere("HORARIO", "LIKE", "%{$search}%")
                ->orWhere("LINK_DETALLE", "LIKE", "%{$search}%")
                ->orWhere("CORREO", "LIKE", "%{$search}%")
                ->orWhere("COORDENADAS", "LIKE", "%{$search}%");

        }
    }
}

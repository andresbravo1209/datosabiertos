<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ResultadoIcfes extends Model
{
    protected $table = 'resultados_icfes';


    protected $fillable = [
        'cole_codigo_icfes',
        'cole_nombre_establecimiento','cole_cod_mcpio_ubicacion',"cole_cod_dane_sede"
    ];

    protected function getResults($request)
    {
        $search = $request->get('search');
        return \App\ResultadoIcfes::search($search)->paginate('20');
    }

    public function scopeSearch($query, $search)
    {
        if(trim($search) != '') {
            $query ->distinct()->select("*")->where("cole_nombre_establecimiento", "LIKE", "%{$search}%")
                ->orderBy("cole_cod_dane_sede");
        }
    }
}

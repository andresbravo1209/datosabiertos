<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vivienda extends Model
{
    protected $table = 'subsidios_de_vivienda_asignados';

    protected $fillable = [
    ];

    protected function getVivienda ($request)
    {
        $search = $request->get('search');
        return \App\Vivienda::search($search)->paginate('7');
    }

    public function scopeSearch($query, $search)
    {
        if(trim($search) != '')
        {
            $query
            ->where("Departamento", "LIKE", "%{$search}%")
            ->orWhere("Municipio", "LIKE", "%{$search}%")
            ->orWhere("Programa", "LIKE", "%{$search}%")
            ->orWhere("Año_Asignacion", "LIKE", "%{$search}%")
            ->orWhere("Postulacion", "LIKE", "%{$search}%")
            ->orWhere("Hogares", "LIKE", "%{$search}%")
            ->orWhere("Valores", "LIKE", "%{$search}%");




}}}

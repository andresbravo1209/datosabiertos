<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AcueductoAntioquia extends Model
{
    protected $table =  'tarifas_acueducto_antioquia';

    protected $fillable = [
    ];

    protected function getTarAcueducto($request)
    {
        $search = $request->get('search');
        return \App\AcueductoAntioquia::search($search)->paginate('7');
    }

    public function scopeSearch($query, $search)
    {
        if(trim($search) != '') {
            $query->where("Municipio", "LIKE", "%{$search}%")
                ->orWhere("Sector", "LIKE", "%{$search}%")
                ->orWhere("Estrato", "LIKE", "%{$search}%")
                ->orWhere("Servicio", "LIKE", "%{$search}%")
                ->orWhere("Cargo_Fijo", "LIKE", "%{$search}%")
                ->orWhere("Cargo_por_Consumo_Menor", "LIKE", "%{$search}%")
                ->orWhere("Cargo_por_Consumo_Mayor", "LIKE", "%{$search}%")
                ->orWhere("Suspensión", "LIKE", "%{$search}%")
                ->orWhere("Reinstalación", "LIKE", "%{$search}%")
                ->orWhere("Reconexión", "LIKE", "%{$search}%")
                ->orWhere("Corte", "LIKE", "%{$search}%")
                ->orWhere("Año", "LIKE", "%{$search}%");

        }
    }
}

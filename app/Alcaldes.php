<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Alcaldes extends Model
{
    protected $table =  'contacto_alcaldes_antioquia';

    protected $fillable = [
    ];

    protected function getMayors($request)
    {
        $search = $request->get('search');
        return \App\Alcaldes::search($search)->paginate('7');
    }

    public function scopeSearch($query, $search)
    {
        if(trim($search) != '') {
            $query->where("ID", "LIKE", "%{$search}%")
                ->orWhere("NOMBRE BIBLIOTECA", "LIKE", "%{$search}%")
                ->orWhere("DIRECCIÓN", "LIKE", "%{$search}%")
                ->orWhere("TELEFONO", "LIKE", "%{$search}%")
                ->orWhere("TIPO", "LIKE", "%{$search}%")
                ->orWhere("COMUNA", "LIKE", "%{$search}%")
                ->orWhere("BARRIO", "LIKE", "%{$search}%")
                ->orWhere("HORARIO", "LIKE", "%{$search}%")
                ->orWhere("LINK_DETALLE", "LIKE", "%{$search}%")
                ->orWhere("CORREO", "LIKE", "%{$search}%")
                ->orWhere("COORDENADAS", "LIKE", "%{$search}%");

        }
    }
}

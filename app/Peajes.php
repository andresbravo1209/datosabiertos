<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Peajes extends Model
{
    protected $table = 'costo_peajes';

    protected $fillable = [
    ];

    protected function getTolls($request)
    {
        $search = $request->get('search');
        return \App\Peajes::search($search)->paginate('20');
    }

    public function scopeSearch($query, $search)
    {
        if(trim($search) != '')
        {
            $query->where("Municipio", "LIKE", "%{$search}%")
            ->orWhere("Departamento", "LIKE", "%{$search}%");


        }
    }
}

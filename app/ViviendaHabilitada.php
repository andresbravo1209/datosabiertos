<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ViviendaHabilitada extends Model
{
    protected $table = 'viviendas_habilitadas';

    protected $fillable = [
    ];

    protected function getHomes($request)
    {
        $search = $request->get('search');
        return \App\ViviendaHabilitada::search($search)->paginate('20');
    }

    public function scopeSearch($query, $search)
    {
        if(trim($search) != '') {
            $query->where("DEPARTAMENTO", "LIKE", "%{$search}%")
                ->orWhere("MUNICIPIO", "LIKE", "%{$search}%")
                ->orWhere("N°_VIVIENDAS_DISPONIBLES", "LIKE", "%{$search}%");
        }
    }
}

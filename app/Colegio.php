<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Colegio extends Model
{
    protected $table = 'listado_colegios_bogota';

    protected $fillable = [
    ];

    protected function getColegio ($request)
    {
        $search = $request->get('search');

        return \App\Colegio::search($search)->paginate('7');
    }
    public function scopeSearch($query, $search)
    {
        if(trim($search) != '')
        {
            $query
            ->where("Año", "LIKE", "%{$search}%")
            ->orWhere("Secretaria", "LIKE", "%{$search}%")
            ->orWhere("Establecimiento", "LIKE", "%{$search}%")
            ->orWhere("Zona", "LIKE", "%{$search}%")
            ->orWhere("Direccion", "LIKE", "%{$search}%")
            ->orWhere("Telefono", "LIKE", "%{$search}%")
            ->orWhere("Niveles", "LIKE", "%{$search}%")
            ->orWhere("Jornada", "LIKE", "%{$search}%")
            ->orWhere("Especialidad", "LIKE", "%{$search}%")
            ->orWhere("Calendario", "LIKE", "%{$search}%")
            ->orWhere("Correo", "LIKE", "%{$search}%");



}}}

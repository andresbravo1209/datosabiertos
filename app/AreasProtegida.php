<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AreasProtegida extends Model
{
    protected $table =  'areas_protegidas_corpoguavio';

    protected $fillable = [
    ];

    protected function getAreasProtegidasss($request)
    {
        $search = $request->get('search');
        return \App\AreasProtegida::search($search)->paginate('7');
    }

    public function scopeSearch($query, $search)
    {
        if(trim($search) != '') {
            $query->where("No.", "LIKE", "%{$search}%")
                ->orWhere("AREA_PROTEGIDA", "LIKE", "%{$search}%")
                ->orWhere("MUNICIPIO", "LIKE", "%{$search}%")
                ->orWhere("AREA_Ha", "LIKE", "%{$search}%")
                ->orWhere("AREA_TOTAL_Ha", "LIKE", "%{$search}%")
                ->orWhere("DECLARATORIA", "LIKE", "%{$search}%")
                ->orWhere("No_ACUERDO_ADOPCION", "LIKE", "%{$search}%")
                ->orWhere("No_ACUERDO_REDELIMITACION", "LIKE", "%{$search}%");

        }
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BienesFuentedeOro extends Model
{
    protected $table =  'bienes_usopublico_fuentedeorometa';

    protected $fillable = [
    ];

    protected function getBienesPublicos($request)
    {
        $search = $request->get('search');
        return \App\BienesFuentedeOro::search($search)->paginate('20');
    }

    public function scopeSearch($query, $search)
    {
        if(trim($search) != '') {
            $query->where("Nombre_Predio", "LIKE", "%{$search}%")
                ->orWhere("Estado", "LIKE", "%{$search}%")
                ->orWhere("N°_Escritura", "LIKE", "%{$search}%")
                ->orWhere("Cédula_Catastral", "LIKE", "%{$search}%")
                ->orWhere("Dirección", "LIKE", "%{$search}%");

        }
    }
}

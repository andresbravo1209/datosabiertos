<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CasosSarampion extends Model
{
    protected $table =  'casos_importados_sarampion';

    protected $fillable = [
    ];

    protected function getCasosSarampion($request)
    {
        $search = $request->get('search');
        return \App\CasosSarampion::search($search)->paginate('7');
    }

    public function scopeSearch($query, $search)
    {
        if(trim($search) != '')
        {
            $query->where("Depto_Notificador", "LIKE", "%{$search}%")
                ->orWhere("Año2019", "LIKE", "%{$search}%")
                ->orWhere("Año2020", "LIKE", "%{$search}%")
                ->orWhere("Total", "LIKE", "%{$search}%");

        }
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Restaurante extends Model
{
    protected $table = 'restaurantes';

    protected $fillable = [
    ];

    protected function getRestaurante ($request)
    {
        $search = $request->get('search');
        return \App\Restaurante::search($search)->paginate('7');
    }

    public function scopeSearch($query, $search)
    {
        if(trim($search) != '')
        {
            $query
            ->where("Nombre", "LIKE", "%{$search}%")
            ->orWhere("Direccion", "LIKE", "%{$search}%")
            ->orWhere("Telefono", "LIKE", "%{$search}%")
            ->orWhere("Localidad", "LIKE", "%{$search}%")
            ->orWhere("Domicilio", "LIKE", "%{$search}%")
            ->orWhere("Tel_Domicilio", "LIKE", "%{$search}%")
            ->orWhere("Pagina_Web", "LIKE", "%{$search}%");




}}}

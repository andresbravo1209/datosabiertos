<?php

namespace App\Http\Controllers;

use App\Alcaldes;
use Illuminate\Http\Request;

class AlcaldesAntioquiaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $mayors = \App\Alcaldes::getMayors($request);
        return view('alcaldes', compact('mayors'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Alcaldes   $alcaldes
     * @return \Illuminate\Http\Response
     */
    public function show(Alcaldes $alcaldes)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Alcaldes  $alcaldes
     * @return \Illuminate\Http\Response
     */
    public function edit(Alcaldes $alcaldes)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Alcaldes  $alcaldes
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Alcaldes $alcaldes)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Alcaldes  $alcaldes
     * @return \Illuminate\Http\Response
     */
    public function destroy(Alcaldes $alcaldes)
    {
        //
    }
}

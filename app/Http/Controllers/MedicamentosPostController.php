<?php

namespace App\Http\Controllers;

use App\MedicamentosPost;
use Illuminate\Http\Request;

class MedicamentosPostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $medicines = \App\MedicamentosPost::getMedicines($request);
        return view('MedicamentosPost', compact('medicines'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\MedicamentosPost  $medicamentosPost
     * @return \Illuminate\Http\Response
     */
    public function show(MedicamentosPost $medicamentosPost)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\MedicamentosPost  $medicamentosPost
     * @return \Illuminate\Http\Response
     */
    public function edit(MedicamentosPost $medicamentosPost)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\MedicamentosPost  $medicamentosPost
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, MedicamentosPost $medicamentosPost)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\MedicamentosPost  $medicamentosPost
     * @return \Illuminate\Http\Response
     */
    public function destroy(MedicamentosPost $medicamentosPost)
    {
        //
    }
}

<?php

namespace App\Http\Controllers;
use App\BibliotecasMedellin;
use Illuminate\Http\Request;

class BibliotecasMedellinController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $bibliotecas=\App\BibliotecasMedellin::getBibliotecasMedellin($request);
        return view('bibliotecasMedellin', compact('bibliotecas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\BibliotecasMedellin $bibliotecas
     * @return \Illuminate\Http\Response
     */
    public function show(BibliotecasMedellin $bibliotecas)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  BibliotecasMedellin  $bibliotecas
     * @return \Illuminate\Http\Response
     */
    public function edit(BibliotecasMedellin $bibliotecas)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  BibliotecasMedellin  $bibliotecas
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, BibliotecasMedellin $bibliotecas)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  BibliotecasMedellin  $bibliotecas
     * @return \Illuminate\Http\Response
     */
    public function destroy(BibliotecasMedellin $bibliotecas)
    {
        //
    }
}

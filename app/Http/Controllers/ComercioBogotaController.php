<?php

namespace App\Http\Controllers;
use \App\ComercioBogota;
use Illuminate\Http\Request;

class ComercioBogotaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $comercioBog= \App\ComercioBogota::getComercioBta($request);
        return view('comercioBogota', compact('comercioBog'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ComercioBogota   $comercioBog
     * @return \Illuminate\Http\Response
     */
    public function show(ComercioBogota  $comercioBog)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ComercioBogota  $comercioBog
     * @return \Illuminate\Http\Response
     */
    public function edit(ComercioBogota  $comercioBog)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ComercioBogota  $comercioBog
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ComercioBogota  $comercioBog)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ComercioBogota  $comercioBog
     * @return \Illuminate\Http\Response
     */
    public function destroy(ComercioBogota  $comercioBog)
    {
        //
    }
}

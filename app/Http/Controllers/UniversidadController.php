<?php

namespace App\Http\Controllers;

use App\Universidad;
use Illuminate\Http\Request;

class UniversidadController extends Controller
{


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
      $coordenadas = [
        'Valle del Cauca' => [-76.5224991, 3.4372201],
        'Antioquia' => [-75.5635900, 6.2518400],
        'Atlántico' => [-75.5000000, 7.0000000],
        'Boyacá' => [-73.3677826, 5.5352802],
        'Bolívar' => [-76.017, 5.85],
        'Caldas' => [-74.6631088, 5.4478302],
        'Caquetá' => [-75.6062317, 1.61438],
        'Cauca' => [-76.6131592, 2.43823],
        'Cesar' => [-73.6166, 8.3084402],
        'Sucre' => [-75.3977814, 9.3047199],
        'Chocó' => [-76.6583481, 5.6918802],
        'Córdoba' => [-75.4201965, 7.9791698],
        'Cundinamarca' => [-74.8075409, 4.3007898],
        'Huila' => [-76.0507126, 1.8537101],
        'La Guajira' => [-72.9072189, 11.5444403],
        'Magdalena' => [-74.1850433, 10.5206604],
        'Meta' => [-73.7579727, 3.9869499],
        'Nariño' => [-78.8155594, 1.79861],
        'Norte de Santander' => [-72.4741669, 7.83389],
        'Quindio' => [-75.6409073, 4.52949],
        'Risaralda' => [-75.6672668, 4.83916],
        'Santander' => [-73.086441, 7.0622201],
        'Tolima' => [-74.8842926, 4.14924]
    ];
        $universidades = \App\Universidad::getUniversidad($request);
        $data=[];
        $data['type'] = "FeatureCollection";
$data['crs'] = [
    'type' => 'name',
    'properties' => [
        'name' => 'urn:ogc:def:crs:OGC:1.3:CRS84'
    ]
];
        foreach ($universidades as $key => $universidad) {
          //echo $universidad;
    $data['features'][$key] = [
        'type' => "Feature",
        'properties' => [
        'cantidad' => $universidad['cantidad']
    ],
        'geometry' => [
            'type' => "Point",
        'coordinates' => [
            $coordenadas[$universidad['DEPARTAMENTO']][0], $coordenadas[$universidad['DEPARTAMENTO']][1]
        ]
        ],
    ];
}

       //echo json_encode($data);

        //die();
        return view('universidad', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Universidad  $medicamentosPost
     * @return \Illuminate\Http\Response
     */
    public function show(Universidad $universidad)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Universidad  $medicamentosPost
     * @return \Illuminate\Http\Response
     */
    public function edit(Universidad $Universidad)
    {
        //colegio
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Universidad  $medicamentosPost
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Universidad $Universidad)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Universidad  $medicamentosPost
     * @return \Illuminate\Http\Response
     */
    public function destroy(Universidad $Universidad)
    {
        //
    }
}

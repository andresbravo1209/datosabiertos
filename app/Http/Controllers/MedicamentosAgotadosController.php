<?php

namespace App\Http\Controllers;

use App\MedicamentosAgotados;
use Illuminate\Http\Request;

class MedicamentosAgotadosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $medicines = \App\MedicamentosAgotados::getMedicines($request);
        return view('medicamentosAgotados', compact('medicines'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\MedicamentosAgotados  $medicamentosAgotados
     * @return \Illuminate\Http\Response
     */
    public function show(MedicamentosAgotados $medicamentosAgotados)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\MedicamentosAgotados  $medicamentosAgotados
     * @return \Illuminate\Http\Response
     */
    public function edit(MedicamentosAgotados $medicamentosAgotados)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\MedicamentosAgotados  $medicamentosAgotados
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, MedicamentosAgotados $medicamentosAgotados)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\MedicamentosAgotados  $medicamentosAgotados
     * @return \Illuminate\Http\Response
     */
    public function destroy(MedicamentosAgotados $medicamentosAgotados)
    {
        //
    }
}

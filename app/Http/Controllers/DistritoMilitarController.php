<?php

namespace App\Http\Controllers;

use App\Militar;
use Illuminate\Http\Request;

class DistritoMilitarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $districts = \App\Militar::getDistricts($request);
        return view('distritoMilitar', compact('districts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Militar  $militar
     * @return \Illuminate\Http\Response
     */
    public function show(Militar $militar)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Militar  $militar
     * @return \Illuminate\Http\Response
     */
    public function edit(Militar $militar)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Militar  $militar
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Militar $militar)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Militar  $militar
     * @return \Illuminate\Http\Response
     */
    public function destroy(Militar $militar)
    {
        //
    }
}

<?php

namespace App\Http\Controllers;

use App\Viviendas;
use Illuminate\Http\Request;

class ViviendasNuevasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $homes= \App\Viviendas::getHomes($request);
        return view('viviendas', compact('homes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Viviendas  $viviendas
     * @return \Illuminate\Http\Response
     */
    public function show(Viviendas $viviendas)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Viviendas  $viviendas
     * @return \Illuminate\Http\Response
     */
    public function edit(Viviendas $viviendas)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Viviendas  $viviendas
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Viviendas $viviendas)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Viviendas  $viviendas
     * @return \Illuminate\Http\Response
     */
    public function destroy(Viviendas $viviendas)
    {
        //
    }
}

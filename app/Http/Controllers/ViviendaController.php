<?php

namespace App\Http\Controllers;

use App\Vivienda;
use Illuminate\Http\Request;

class ViviendaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $viviendas = \App\Vivienda::getVivienda($request);
        return view('vivienda', compact('viviendas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Vivienda  $medicamentosPost
     * @return \Illuminate\Http\Response
     */
    public function show(Vivienda $vivienda)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Vivienda  $medicamentosPost
     * @return \Illuminate\Http\Response
     */
    public function edit(Vivienda $vivienda)
    {
        //colegio
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Vivienda  $medicamentosPost
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Vivienda $vivienda)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Restaurante  $medicamentosPost
     * @return \Illuminate\Http\Response
     */
    public function destroy(Vivienda $vivienda)
    {
        //
    }
}

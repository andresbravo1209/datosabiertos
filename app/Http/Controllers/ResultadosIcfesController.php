<?php

namespace App\Http\Controllers;

use App\ResultadoIcfes;
use Illuminate\Http\Request;

class ResultadosIcfesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $results = \App\ResultadoIcfes::getResults($request);
        //dd($results);
        return view('educacion', compact('results'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ResultadoIcfes  $resultadoIcfes
     * @return \Illuminate\Http\Response
     */
    public function show(ResultadoIcfes $resultadoIcfes)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ResultadoIcfes  $resultadoIcfes
     * @return \Illuminate\Http\Response
     */
    public function edit(ResultadoIcfes $resultadoIcfes)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ResultadoIcfes  $resultadoIcfes
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ResultadoIcfes $resultadoIcfes)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ResultadoIcfes  $resultadoIcfes
     * @return \Illuminate\Http\Response
     */
    public function destroy(ResultadoIcfes $resultadoIcfes)
    {
        //
    }
}

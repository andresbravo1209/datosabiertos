<?php

namespace App\Http\Controllers;

use App\CasosSarampion;
use Illuminate\Http\Request;

class CasosSarampionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $casosSarampion = \App\CasosSarampion::getCasosSarampion($request);
        return view('casosImportadosSarampion',compact('casosSarampion'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CasosSarampion $casosSarampion
     * @return \Illuminate\Http\Response
     */
    public function show(CasosSarampion  $casosSarampion)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CasosSarampion  $casosSarampion
     * @return \Illuminate\Http\Response
     */
    public function edit(CasosSarampion  $casosSarampion)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CasosSarampion  $casosSarampion
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CasosSarampion  $casosSarampion)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CasosSarampion  $casosSarampion
     * @return \Illuminate\Http\Response
     */
    public function destroy(CasosSarampion  $casosSarampion)
    {
        //
    }
}

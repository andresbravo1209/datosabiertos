<?php

namespace App\Http\Controllers;

use App\Leyes;
use Illuminate\Http\Request;

class LeyesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $laws = \App\Leyes::getLaws($request);
        return view('leyes', compact('laws'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Leyes  $leyes
     * @return \Illuminate\Http\Response
     */
    public function show(Leyes $leyes)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Leyes  $leyes
     * @return \Illuminate\Http\Response
     */
    public function edit(Leyes $leyes)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Leyes  $leyes
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Leyes $leyes)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Leyes  $leyes
     * @return \Illuminate\Http\Response
     */
    public function destroy(Leyes $leyes)
    {
        //
    }
}

<?php

namespace App\Http\Controllers;

use App\Sisben;
use Illuminate\Http\Request;

class SisbenController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $persons = \App\Sisben::getPersons($request);
        return view('sisben', compact('persons'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Sisben  $sisben
     * @return \Illuminate\Http\Response
     */
    public function show(Sisben $sisben)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Sisben  $sisben
     * @return \Illuminate\Http\Response
     */
    public function edit(Sisben $sisben)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Sisben  $sisben
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Sisben $sisben)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Sisben  $sisben
     * @return \Illuminate\Http\Response
     */
    public function destroy(Sisben $sisben)
    {
        //
    }
}

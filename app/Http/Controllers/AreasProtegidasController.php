<?php

namespace App\Http\Controllers;
use App\AreasProtegida;
use Illuminate\Http\Request;

class AreasProtegidasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $areasProtegida=\App\AreasProtegida::getAreasProtegidasss($request);
        return view('areasCorpoguavioView',compact('areasProtegida'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\AreasProtegida   $areasProtegida
     * @return \Illuminate\Http\Response
     */
    public function show(AreasProtegida $areasProtegida)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\AreasProtegida  $areasProtegida
     * @return \Illuminate\Http\Response
     */
    public function edit(AreasProtegida $areasProtegida)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\AreasProtegida  $areasProtegida
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AreasProtegida  $areasProtegida)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AreasProtegida  $areasProtegida
     * @return \Illuminate\Http\Response
     */
    public function destroy(AreasProtegida  $areasProtegida)
    {
        //
    }
}

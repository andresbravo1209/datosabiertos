<?php

namespace App\Http\Controllers;

use App\Peajes;
use Illuminate\Http\Request;

class PeajesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $tolls = \App\Peajes::getTolls($request);
        return view('peajes', compact('tolls'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Peajes  $peajes
     * @return \Illuminate\Http\Response
     */
    public function show(Peajes $peajes)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Peajes  $peajes
     * @return \Illuminate\Http\Response
     */
    public function edit(Peajes $peajes)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Peajes  $peajes
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Peajes $peajes)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Peajes  $peajes
     * @return \Illuminate\Http\Response
     */
    public function destroy(Peajes $peajes)
    {
        //
    }
}

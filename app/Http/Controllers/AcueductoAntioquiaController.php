<?php

namespace App\Http\Controllers;

use App\AcueductoAntioquia;
use Illuminate\Http\Request;

class AcueductoAntioquiaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $tarAcueducto= \App\AcueductoAntioquia::getTarAcueducto($request);
        return view('acueductoAntioquia', compact('tarAcueducto'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\AcueductoAntioquia  $tarAcueducto
     * @return \Illuminate\Http\Response
     */
    public function show(AcueductoAntioquia  $tarAcueducto)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\AcueductoAntioquia  $tarAcueducto
     * @return \Illuminate\Http\Response
     */
    public function edit(AcueductoAntioquia $tarAcueducto)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\AcueductoAntioquia  $tarAcueducto
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AcueductoAntioquia $tarAcueducto)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AcueductoAntioquia  $tarAcueducto
     * @return \Illuminate\Http\Response
     */
    public function destroy(AcueductoAntioquia $tarAcueducto)
    {
        //
    }
}

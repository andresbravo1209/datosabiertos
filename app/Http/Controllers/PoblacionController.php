<?php

namespace App\Http\Controllers;

use App\Poblacion;
use Illuminate\Http\Request;

class PoblacionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $poblaciones = \App\Poblacion::getPoblacion($request);
        return view('poblacion', compact('poblaciones'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Poblacion  $medicamentosPost
     * @return \Illuminate\Http\Response
     */
    public function show(Poblacion $poblacion)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Poblacion  $medicamentosPost
     * @return \Illuminate\Http\Response
     */
    public function edit(Poblacion $poblacion)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Poblacion  $medicamentosPost
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Poblacion $poblacion)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Poblacion  $medicamentosPost
     * @return \Illuminate\Http\Response
     */
    public function destroy(Poblacion $poblacion)
    {
        //
    }
}

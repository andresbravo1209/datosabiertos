<?php

namespace App\Http\Controllers;
use \App\BienesFuentedeOro;
use Illuminate\Http\Request;

class BienesFuentedeOroController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $bienesPublicos=\App\BienesFuentedeOro::getBienesPublicos($request);
        return view('bienesFuentedeOro',compact('bienesPublicos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\BienesFuentedeOro  $bienesPublicos
     * @return \Illuminate\Http\Response
     */
    public function show(BienesFuentedeOro  $bienesPublicos)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\BienesFuentedeOro  $bienesPublicos
     * @return \Illuminate\Http\Response
     */
    public function edit(BienesFuentedeOro $bienesPublicos)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\BienesFuentedeOro  $bienesPublicos
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, BienesFuentedeOro $bienesPublicos)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\BienesFuentedeOro  $bienesPublicos
     * @return \Illuminate\Http\Response
     */
    public function destroy(BienesFuentedeOro $bienesPublicos)
    {
        //
    }
}

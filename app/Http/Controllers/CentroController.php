<?php

namespace App\Http\Controllers;

use App\Centro;
use Illuminate\Http\Request;

class CentroController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $centros = \App\Centro::getCentro($request);
        return view('centro', compact('centros'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Colegio  $medicamentosPost
     * @return \Illuminate\Http\Response
     */
    public function show(Colegio $colegio)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Colegio  $medicamentosPost
     * @return \Illuminate\Http\Response
     */
    public function edit(Colegio $colegio)
    {
        //colegio
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Colegio  $medicamentosPost
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Colegio $colegio)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Colegio  $medicamentosPost
     * @return \Illuminate\Http\Response
     */
    public function destroy(Colegio $colegio)
    {
        //
    }
}

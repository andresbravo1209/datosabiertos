<?php

namespace App\Http\Controllers;

use App\CuadrantePolicial;
use Illuminate\Http\Request;

class CuadrantePolicialController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
      //  dd("hola mundo");
        $polices = \App\CuadrantePolicial::getPolices($request);
        return view('cuadrante', compact('polices'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CuadrantePolicial  $cuadrantePolicial
     * @return \Illuminate\Http\Response
     */
    public function show(CuadrantePolicial $cuadrantePolicial)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CuadrantePolicial  $cuadrantePolicial
     * @return \Illuminate\Http\Response
     */
    public function edit(CuadrantePolicial $cuadrantePolicial)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CuadrantePolicial  $cuadrantePolicial
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CuadrantePolicial $cuadrantePolicial)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CuadrantePolicial  $cuadrantePolicial
     * @return \Illuminate\Http\Response
     */
    public function destroy(CuadrantePolicial $cuadrantePolicial)
    {
        //
    }
}

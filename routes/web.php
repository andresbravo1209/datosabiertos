<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/dashboard', 'DashboardController@index')->name('dashboard');
Route::resource('/salud', 'SaludController')->only('index', 'store', 'update', 'destroy');
Route::resource('/vivienda', 'ViviendaController')->only('index', 'store', 'update', 'destroy');
Route::resource('/movilidad', 'MovilidadController')->only('index', 'store', 'update', 'destroy');
Route::resource('/results-icfes', 'ResultadosIcfesController')->only('index', 'store', 'update', 'destroy');
Route::resource('/contratacion', 'ContratacionController')->only('index', 'store', 'update', 'destroy');
Route::resource('/cuadrante-policial','CuadrantePolicialController')->only('index','store','update','destroy');
Route::resource('/medicamentos-agotados','MedicamentosAgotadosController')->only('index','store','update','destroy');
Route::resource('/alcaldes-antioquia','AlcaldesAntioquiaController')->only('index','store','update','destroy');
Route::resource('/medicamentos-post','MedicamentosPostController')->only('index','store','update','destroy');
Route::resource('/leyes','LeyesController')->only('index','store','update','destroy');
Route::resource('/peajes','PeajesController')->only('index','store','update','destroy');
Route::resource('/educacion-basica','EducacionBasicaController')->only('index','store','update','destroy');
Route::resource('/distrito-militar','DistritoMilitarController')->only('index','store','update','destroy');
Route::resource('/camara-comercio','CamaraDeComercioController')->only('index','store','update','destroy');
Route::resource('/becas','BecasController')->only('index','store','update','destroy');
Route::resource('/estacion-gas','EstacionesGasController')->only('index','store','update','destroy');
Route::resource('/internet-para-la-gente','InternetController')->only('index','store','update','destroy');
Route::resource('/sisben','SisbenController')->only('index','store','update','destroy');
Route::resource('/empleados','FondoEmpleadosController')->only('index','store','update','destroy');
Route::resource('/paraderos','ParaderosSitpController')->only('index','store','update','destroy');
Route::resource('/viviendas-nuevas','ViviendasNuevasController')->only('index','store','update','destroy');
Route::resource('/institutos-bogota','InstitutosBogotaController')->only('index','store','update','destroy');
Route::resource('/acueducto-antioquia','AcueductoAntioquiaController')->only('index','store','update','destroy');
Route::resource('/poblacion','PoblacionController')->only('index','store','update','destroy');
Route::resource('/universidad','universidadController')->only('index','store','update','destroy');
Route::resource('/colegio','ColegioController')->only('index','store','update','destroy');
Route::resource('/restaurante','restauranteController')->only('index','store','update','destroy');
Route::resource('/vivienda','viviendaController')->only('index','store','update','destroy');
/*Route::resource('/covid','AcueductoAntioquiaController')->only('index','store','update','destroy');*/
Route::resource('/testing','TestingController')->only('index','store','update','destroy');
Route::resource('/areas','AreasProtegidasController')->only('index','store','update','destroy');
Route::resource('/bibliotecas-medellin','BibliotecasMedellinController')->only('index','store','update','destroy');
Route::resource('/bienesPublicos-FuentedeOro','BienesFuentedeOroController')->only('index','store','update','destroy');
Route::resource('/casos-sarampion','CasosSarampionController')->only('index','store','update','destroy');
Route::resource('/testing','TestingController')->only('index','store','update','destroy');
Route::resource('/areas','AreasProtegidasController')->only('index','store','update','destroy');
Route::resource('/bibliotecas-medellin','BibliotecasMedellinController')->only('index','store','update','destroy');
Route::resource('/bienesPublicos-FuentedeOro','BienesFuentedeOroController')->only('index','store','update','destroy');
Route::resource('/casos-sarampion','CasosSarampionController')->only('index','store','update','destroy');
Route::resource('/covid','CovidController')->only('index','store','update','destroy');
Route::resource('/centro','CentroController')->only('index','store','update','destroy');

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Servicios Inteligentes | Dashboard</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="{{ asset('plugins/fontawesome-free/css/all.min.css') }}">
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <link rel="stylesheet" type="text/css" href="{{ asset('plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('plugins/jqvmap/jqvmap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('dist/css/adminlte.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('plugins/overlayScrollbars/css/OverlayScrollbars.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('plugins/daterangepicker/daterangepicker.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('plugins/summernote/summernote-bs4.css') }}">
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">

    <!-- Navbar -->
    <nav class="main-header navbar navbar-expand navbar-white navbar-light">
        <!-- Left navbar links -->
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
            </li>
            <li class="nav-item d-none d-sm-inline-block">
                <a href="/" class="nav-link">Home</a>
            </li>
        </ul>
    </nav>
    <!-- /.navbar -->
    <!-- Main Sidebar Container -->
    <aside class="main-sidebar sidebar-dark-primary elevation-4">
        <!-- Brand Logo -->
        <a href="/" class="brand-link">
            <img src="dist/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
                 style="opacity: .8">
            <span class="brand-text font-weight-light">Servicios Inteligentes</span>
        </a>
        <!-- Sidebar -->
        <div class="sidebar">
            <!-- Sidebar user panel (optional) -->
            <div class="user-panel mt-3 pb-3 mb-3 d-flex">
                <div class="image">
                    <img src="dist/img/user2-160x160.jpg" class="img-circle elevation-2" alt="User Image">
                </div>
                <div class="info">
                    <a href="#" class="d-block">Bienvenido!</a>
                </div>
            </div>
            <!-- Sidebar Menu -->
            <nav class="mt-2">
                <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                    <li class="nav-header">SERVICIOS</li>
                    <li class="nav-item has-treeview">
                        <a href="#" class="nav-link">
                            <i class="fas fa-hospital"></i>
                            <p>Salud
                                <i class="fas fa-angle-left right"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview" style="display: none;">
                            <li class="nav-item">
                                <a href="/medicamentos-agotados" class="nav-link">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Medicamentos Agotados</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="/medicamentos-post" class="nav-link">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Medicamentos post</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="/sisben" class="nav-link">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Sisben</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="/salud" class="nav-link">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Salud</p>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-item has-treeview">
                        <a href="#" class="nav-link">
                            <i class="fas fa-home"></i>
                            <p>
                                Vivienda
                                <i class="fas fa-angle-left right"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview" style="display: none;">
                            <li class="nav-item">
                                <a href="/vivienda" class="nav-link">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Comprar viviendas usadas</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="/viviendas-nuevas" class="nav-link">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Comprar viviendas nuevas</p>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-item has-treeview">
                        <a href="#" class="nav-link">
                            <i class="fas fa-business-time"></i>
                            <p>
                                Privados
                                <i class="fas fa-angle-left right"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview" style="display: none;">
                            <li class="nav-item">
                                <a href="/companies" class="nav-link">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>1000 Empresas Colombianas</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="/acueducto-antioquia" class="nav-link">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Tarifas Acueducto Antioquia</p>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-item has-treeview">
                        <a href="#" class="nav-link">
                            <i class="fas fa-car"></i>
                            <p>
                                Movilidad
                                <i class="fas fa-angle-left right"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview" style="display: none;">
                            <li class="nav-item">
                                <a href="/movilidad" class="nav-link">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Gasolineras cercanas</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="/peajes" class="nav-link">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Costo de peajes</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="/estacion-gas" class="nav-link">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Estaciones en carretera</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="/paraderos" class="nav-link">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Paraderos de SITP</p>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-item has-treeview">
                        <a href="#" class="nav-link">
                            <i class="fas fa-shield-alt"></i>
                            <p>
                                Seguridad
                                <i class="fas fa-angle-left right"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview" style="display: none;">
                            <li class="nav-item">
                                <a href="/cuadrante-policial" class="nav-link">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Cuadrantes Policia Bogotá</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="/distrito-militar" class="nav-link">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Distritos Militares</p>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-item has-treeview">
                        <a href="#" class="nav-link">
                            <i class="fas fa-school"></i>
                            <p>
                                Educación
                                <i class="fas fa-angle-left right"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview" style="display: none;">
                            <li class="nav-item">
                                <a href="/results-icfes" class="nav-link">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Resultados ICFES 2018</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="/educacion-basica" class="nav-link">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Porcentajes Matriculas</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="/becas" class="nav-link">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Accede a Becas</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="/colegios-bogota" class="nav-link">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Colegios de Bogotá</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="/institutos-bogota" class="nav-link">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Institutos de Bogotá</p>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-item has-treeview">
                        <a href="#" class="nav-link">
                            <i class="fas fa-info-circle"></i>
                            <p>
                                Información Publica
                                <i class="fas fa-angle-left right"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview" style="display: none;">
                            <li class="nav-item">
                                <a href="/contratacion" class="nav-link">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Contrataciones</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="/alcaldes-antioquia" class="nav-link">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Alcaldia Antioquía</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="/leyes" class="nav-link">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Consulta de Leyes</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="/camara-comercio" class="nav-link">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Camara de Comercio</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="/internet-para-la-gente" class="nav-link">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Internet para la gente</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="/empleados" class="nav-link">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Fondo de Empleados</p>
                                </a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </nav>
        </div>
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">Contratación</h1>
                        <h1 class="m-0 text-dark">Se el veedor de tu ciudad.</h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active">Dashboard</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <!-- Small boxes (Stat box) -->
                <div class="card">
                    <form class="form-horizontal">
                        <div class="card-body">
                            <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-2 col-form-label">Proyectos en especial: </label>
                                <div class="col-sm-10">
                                    {!! Form::open(['route' => 'movilidad.index', 'method' => 'GET', 'class' => 'input-icon my-3 my-lg-0']) !!}
                                    {!! Form::search('search', null, ['class' => 'form-control', 'placeholder' => 'Mas información']) !!}
                                    {!! Form::close() !!}
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Información encontrada</h3>
                    </div>
                    <!-- /.card-header -->
                    @if($contracts->isEmpty())
                        <div class="card-body">
                            <div class="alert alert-info">No hay informacion para mostrar</div>
                        </div>
                    @else
                        <div class="card-body p-0">
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th style="width: 10px">Nombre Proyecto</th>
                                    <th>Descripción</th>
                                    <th>Organización</th>
                                    <th>Info Adicional</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($contracts as $contract)
                                    <tr>
                                        <td>
                                            {!! $contract->Nombre_del_proyecto  !!}
                                        </td>
                                        <td>
                                            {!! Form::textarea('keterangan', $contract->Descripción, ['readonly', 'class' => 'form-control']) !!}
                                        </td>
                                        <td>
                                            {!! $contract->Organización !!}

                                        </td>
                                        <td>
                                            <span class='text-right text-dark'>Email contacto: </span><small class='text-muted'>{!! isset($contract->Email_de_contacto)? $contract->Email_de_contacto : '' !!}</small>
                                            <br>
                                            <span class='text-right text-dark'>URL Codigo: </span> <small class='text-muted'>{!! isset($contract->URL_Código)? $contract->URL_Código : '' !!}</small>
                                            <br>
                                            <span class='text-right text-dark'>URL Video: </span> <small class='text-muted'>{!! isset($contract->URL_Video)? $contract->URL_Video : '' !!}</small>
                                            <br>
                                            <a href= {!! isset($contract->URL_Documentación)? $contract->URL_Documentación : '' !!}>Documentación</a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    @endif
                    <div class="card-footer">
                        <div class="row">
                            <div class="col">
                                {!! $contracts->appends(['search' => request()->get('search')])->render() !!}
                            </div>
                        </div>
                    </div>
                    <!-- /.card-body -->
                </div>{{--FIN DE SECCION--}}
            </div>
        </section>
        <section class="col-lg-5 connectedSortable">
        </section>
    </div>
</div>
<footer class="main-footer">
    <strong>Smart cities &copy; 2019 <a href="http://adminlte.io">UPC</a>.</strong>
    All rights reserved.
    <div class="float-right d-none d-sm-inline-block">
        <b>Version</b> 0.0.1
    </div>
</footer>
<aside class="control-sidebar control-sidebar-dark">
</aside>
<!-- jQuery -->
<script src="{{ asset('plugins/jquery/jquery.min.js') }}"></script>
<!-- jQuery UI 1.11.4 -->
<script src="{{ asset('plugins/jquery-ui/jquery-ui.min.js') }}"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
    $.widget.bridge('uibutton', $.ui.button)
</script>
<!-- Bootstrap 4 -->
<script src="{{ asset('plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('dist/js/adminlte.js') }}"></script>
</body>
</html>


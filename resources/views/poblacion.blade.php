<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <title>Bell Bootstrap 4 Theme</title>
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta content="" name="keywords">
  <meta content="" name="description">

  <!-- Facebook Opengraph integration: https://developers.facebook.com/docs/sharing/opengraph -->
  <meta property="og:title" content="">
  <meta property="og:image" content="">
  <meta property="og:url" content="">
  <meta property="og:site_name" content="">
  <meta property="og:description" content="">

  <!-- Twitter Cards integration: https://dev.twitter.com/cards/  -->
  <meta name="twitter:card" content="summary">
  <meta name="twitter:site" content="">
  <meta name="twitter:title" content="">
  <meta name="twitter:description" content="">
  <meta name="twitter:image" content="">


  <!-- Favicon -->
  <link href="img/favicon.ico" rel="icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Raleway:400,500,700|Roboto:400,900" rel="stylesheet">

  <!-- Bootstrap CSS File -->
  <link href="lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Libraries CSS Files -->
  <link href="lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">

  <!-- Main Stylesheet File -->
  <link href="css/style.css" rel="stylesheet">

  <!-- =======================================================
    Theme Name: Bell
    Theme URL: https://bootstrapmade.com/bell-free-bootstrap-4-template/
    Author: BootstrapMade.com
    Author URL: https://bootstrapmade.com
  ======================================================= -->
</head>

<body>
  <!-- Page Content
    ================================================== -->
  <!-- Hero -->

  <section class="hero">
    <div class="container text-center">
      <div class="row mb-2">
                    <div class="col-sm-6">
                      <div class="container text-center">
                        <h1 >Casos Covid-19</h1>
                    </div>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="/">Home</a></li>

                        </ol>
                    </div>
                      </div>
                      <form class="form-horizontal">
                                                                    <div class="card-body">
                                                                        <div class="form-group row">
                                                                          
                                                                            <div class="col-sm-10">
                                                                                {!! Form::open(['route' => 'poblacion.index', 'method' => 'GET', 'class' => 'input-icon my-3 my-lg-0']) !!}
                                                                                {!! Form::search('search', null, ['class' => 'form-control', 'placeholder' => 'Palabras clave']) !!}
                                                                                {!! Form::close() !!}
                                                                            </div>

                                                                        </div>
                                                                    </div>
                                                                </form>
<div class="card-footer">
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th>Ciudad</th>
                                    <th>Departamento</th>
                                    <th>Atencion</th>
                                    <th>Edad</th>
                                    <th>Sexo</th>
                                    <th>Tipo</th>
                                    <th>Estado</th>
                                    <th>Procedencia</th>
                                    <th>Fecha Muerte</th>
                                    <th>Fecha Diagnostico</th>
                                    <th>Fecha Recuperado</th>

                                </tr>
                                </thead>
                                <tbody>
                                @foreach($poblaciones as $poblacion)
                                    <tr>
                                        <td>
                                            {!! $poblacion->Ciudad!!}
                                        </td>
                                        <td>
                                            {!! $poblacion->Departamento_Distrito !!}

                                        </td>

                                        <td>
                                            {!! $poblacion->Atencion !!}
                                        </td>

                                        <td>
                                            {!! $poblacion->Edad !!}
                                        </td>

                                        <td>
                                            {!! $poblacion->Sexo !!}
                                        </td>
                                        <td>
                                            {!! $poblacion->Tipo !!}
                                        </td>
                                        <td>
                                            {!! $poblacion->Estado !!}
                                        </td>
                                        <td>
                                            {!! $poblacion->	País_Procedencia !!}
                                        </td>
                                        <td>
                                            {!! $poblacion->Fecha_Muerte !!}
                                        </td>
                                        <td>
                                            {!! $poblacion->Fecha_Diagnostico !!}
                                        </td>
                                        <td>
                                            {!! $poblacion->Fecha_Recuperado !!}
                                        </td>


                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>

                        <div class="row">
                          <div class="container text-center">
              {!! $poblaciones->appends(['search' => request()->get('search')])->render() !!}
                    </div>
                    </div>

                        </div>





  </section>
  <!-- /Hero -->

  <!-- Header -->
  <header id="header">

  </header>




  <!-- Required JavaScript Libraries -->
  <script src="lib/jquery/jquery.min.js"></script>
  <script src="lib/jquery/jquery-migrate.min.js"></script>
  <script src="lib/superfish/hoverIntent.js"></script>
  <script src="lib/superfish/superfish.min.js"></script>
  <script src="lib/tether/js/tether.min.js"></script>
  <script src="lib/stellar/stellar.min.js"></script>
  <script src="lib/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="lib/counterup/counterup.min.js"></script>
  <script src="lib/waypoints/waypoints.min.js"></script>
  <script src="lib/easing/easing.js"></script>
  <script src="lib/stickyjs/sticky.js"></script>
  <script src="lib/parallax/parallax.js"></script>
  <script src="lib/lockfixed/lockfixed.min.js"></script>

  <!-- Template Specisifc Custom Javascript File -->
  <script src="js/custom.js"></script>

  <script src="contactform/contactform.js"></script>

</body>
</html>

<!DOCTYPE html>

<html lang="en">
<head>

     <title>Servicios Inteligentes</title>
<!--

Eatery Cafe Template

http://www.templatemo.com/tm-515-eatery

-->
     <meta charset="UTF-8">
     <meta http-equiv="X-UA-Compatible" content="IE=Edge">
     <meta name="description" content="">
     <meta name="keywords" content="">
     <meta name="author" content="">
     <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

     <link rel="stylesheet" href="css/bootstrap.min.css">
     <link rel="stylesheet" href="css/font-awesome.min.css">
     <link rel="stylesheet" href="css/animate.css">
     <link rel="stylesheet" href="css/owl.carousel.css">
     <link rel="stylesheet" href="css/owl.theme.default.min.css">
     <link rel="stylesheet" href="css/magnific-popup.css">

     <!-- MAIN CSS -->
     <link rel="stylesheet" href="css/templatemo-style1.css">

</head>
<body>

     <!-- PRE LOADER -->
     <section class="preloader">
          <div class="spinner">

               <span class="spinner-rotate"></span>

          </div>
     </section>


     <!-- MENU -->
     <section class="navbar custom-navbar navbar-fixed-top" role="navigation">
          <div class="container">

               <div class="navbar-header">
                    <button class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">

                    </button>

                    <!-- lOGO TEXT HERE -->
                    <a href="/#" class="navbar-brand">Portal <span>.</span> Inteligente</a>
               </div>

               <div class="collapse navbar-collapse">
                   <ul class="nav navbar-nav navbar-right">
                           @if (Route::has('login'))
                               <div class="top-right links">
                                   @auth
                                       <a href="{{ url('/home') }}">Home</a>
                                   @else
                                       <a href="{{ route('login') }}"class="section-btn btn btn-default smoothScroll" >Inicia Sesión</a>


                                       @if (Route::has('register'))
                                           <a href="{{ route('register') }}" class="section-btn btn btn-default smoothScroll" >Registrate</a>
                                       @endif
                                   @endauth
                               </div>
                           @endif
                </div>
                </ul>

          </div>
     </section>


     <!-- HOME -->
     <section id="home" class="slider" data-stellar-background-ratio="0.5">
          <div class="row">

                    <div class="owl-carousel owl-theme" >
                         <div class="item item-first" style="background-image: URL(https://upload.wikimedia.org/wikipedia/commons/3/34/COLOMBIA_BANDERA.jpg); background-size: 100% 100%;" >
                              <div class="caption" >
                                   <div class="container">
                                        <div class="col-md-8 col-sm-12" >
                                             <h3></h3>
                                             <h1></h1>
                                             <a href="/dashboard" class="section-btn btn btn-default smoothScroll">Ingresar</a>
                                             <div class="col-md-4 col-sm-8">
                                        </div>
                                   </div>
                              </div>
                              <div class="col-md-3">
                                    <div class="right-banner-content">
                                        <h2>¿QUE ES ESTE PORTAL?</h2>
                                        <span>Este portal muestra datos que ha venido recogiendo el gobierno en las diferentes entidades y presenta información que es útil para todos los ciudadanos. Así mismo da algunos lineamientos para facilitar tramites básicos para un ciudadano, según su interes como son la consulta de algunos datos personales más buscados por las personas </span>
                                        <div class="line-dec"></div>
                                    </div>
                                </div>
                            </div>

                         </div>

                         <div class="item item-second" style="background-image: URL(https://www.constructordemarcas.com/wp-content/uploads/2019/09/UNIPILOTO-CASO-1.jpg); background-size: 100% 100%;">
                              <div class="caption">
                                   <div class="container">
                                        <div class="col-md-8 col-sm-12">
                                             <h3></h3>
                                             <h1></h1>
                                             <a href="/dashboard" class="section-btn btn btn-default smoothScroll">Ingresar</a>

                                        </div>
                                   </div>
                                   <div class="col-md-3">
                                         <div class="right-banner-content" style="color: #ffffff" >
                                             <h2>¿QUE ES ESTE PORTAL?</h2>
                                             <span>Este portal muestra datos que ha venido recogiendo el gobierno en las diferentes entidades y presenta información que es útil para todos los ciudadanos. Así mismo da algunos lineamientos para facilitar tramites básicos para un ciudadano, según su interes como son la consulta de algunos datos personales más buscados por las personas </span>
                                             <div class="line-dec"></div>
                                         </div>
                                     </div>

                              </div>
                         </div>

          </div>


     <!-- SCRIPTS -->
     <script src="js/jquery.js"></script>
     <script src="js/bootstrap.min.js"></script>
     <script src="js/jquery.stellar.min.js"></script>
     <script src="js/wow.min.js"></script>
     <script src="js/owl.carousel.min.js"></script>
     <script src="js/jquery.magnific-popup.min.js"></script>
     <script src="js/smoothscroll.js"></script>
     <script src="js/custom.js"></script>

</body>
</html>

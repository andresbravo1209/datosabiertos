<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Servicios</title>
<!--
Avalon Template
http://www.templatemo.com/tm-513-avalon
-->
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="apple-touch-icon" href="apple-touch-icon.png">

        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/bootstrap-theme.min.css">
        <link rel="stylesheet" href="css/fontAwesome.css">
        <link rel="stylesheet" href="css/hero-slider.css">
        <link rel="stylesheet" href="css/owl-carousel.css">
        <link rel="stylesheet" href="css/datepicker.css">
        <link rel="stylesheet" href="css/templatemo-style.css">

        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">

        <script src="js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
    </head>

<body>
  <section class="contact-us" id="contact-section">
      <div class="container-fluid">
          <div class="row">
              <div class="col-md-8">
                  <form id="contact" action="" method="post">
                      <div class="row">
                        <div class="white-button">
                            <a href="https://www.datos.gov.co/">Ingresar</a>

                        </div>

                      </div>
                  </form>
              </div>
              <div class="col-md-4">
                  <div class="contact-right-content">
                      <div class="icon"><img src="img/map-marker-icon.png" alt=""></div>
                      <h4>You can find us on maps</h4>
                  </div>
              </div>
          </div>
      </div>
  </section>

    <section class="banner" id="top">
        <div class="container-fluid">
            <div class="row">
              <div class="col-md-4">
                    <div class="right-banner-content">
                        <div class="logo"><a href="index.html"><img src="img/logo.png" alt=""></a></div>
                        <h2>Contactanos</h2>
                        <span>Redes Sociales </span>
                        <div class="line-dec"></div>

                        <ul class="social-icons">
                            <li><a href="https://www.unipiloto.edu.co/"><i class="fa fa-facebook"></i></a></li>
                            <li><a href=""><i class="fa fa-twitter"></i></a></li>
                            <li><a href="https://www.instagram.com/unipiloto/?hl=es-la"><i class="fa fa-linkedin"></i></a></li>
                            <li><a href="#"><i class="fa fa-rss"></i></a></li>
                            <li><a href="#"><i class="fa fa-dribbble"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="left-banner-content">
                        <div class="text-content">
                            <h6>Covid-19</h6>
                            <div class="line-dec"></div>
                            <h1>Encuentra informacion de las estadisticas del Covid-19 en Colombia y en el mundo</h1>
                            <div class="white-border-button">
                                <a href="https://www.worldometers.info/">Ingresar</a>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>
    <section class="events" id="events-section">
        <div class="content-wrapper">
            <div class="inner-container container-fluid">
                <div class="row">
                    <div class="col-md-12 col-sm-12">
                        <div class="section-heading">
                            <div class="filter-categories">
                                <ul class="project-filter">
                                    <li class="filter" data-filter="all"><span>Salud</span></li>
                                    <li class="filter" data-filter="design"><span>Educación</span></li>
                                    <li class="filter" data-filter="start"><span>Vivienda</span></li>
                                    <li class="filter" data-filter="web"><span>Movilidad</span></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-10 col-sm-12 col-md-offset-1">
                        <div class="projects-holder">
                            <div class="event-list">
                                <ul>
                                    <li class="project-item first-child mix web">
                                        <ul class="event-item web">
                                            <li>
                                                <div class="date">
                                                    <span>29<br>Mayo</span>
                                                </div>
                                            </li>
                                            <li>
                                                <h4>Casos Importados Sarampión</h4>
                                                <div class="web">
                                                    <span>Conjunto de Datos</span>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="time">
                                                    <span>Colombia</span>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="white-button">
                                                    <a href="/casos-sarampion">Ingresar</a>
                                                </div>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class="project-item second-child mix design">
                                        <ul class="event-item design">
                                            <li>
                                                <div class="date">
                                                    <span>29<br>Mayo</span>
                                                </div>
                                            </li>
                                            <li>
                                                <h4>Casos Positivos Covid-19</h4>
                                                <div class="design">
                                                    <span>Conjunto de Datos</span>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="time">
                                                    <span>Colombia</span>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="white-button">
                                                    <a href="/covid">Ingresar</a>
                                                </div>
                                            </li>
                                        </ul>
                                    </li>

                                    <li class="project-item third-child mix start design">
                                        <ul class="event-item start">
                                            <li>
                                                <div class="date">
                                                    <span>29<br>Mayo</span>
                                                </div>
                                            </li>
                                            <li>
                                                <h4>Centros de Salud</h4>
                                                <div class="app">
                                                    <span>Conjunto de Datos</span>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="time">
                                                    <span>Bogota</span>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="white-button">
                                                    <a href="/centro">ingresar</a>
                                                </div>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class="project-item fourth-child mix web">
                                        <ul class="event-item web">
                                            <li>
                                                <div class="date">
                                                    <span>22<br>Mar</span>
                                                </div>
                                            </li>
                                            <li>
                                                <h4>palo santo art party</h4>
                                                <div class="web">
                                                    <span>Web Conferences</span>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="time">
                                                    <span>10:00 AM - 05:00 PM<br>Thursday</span>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="white-button">
                                                    <a href="#">I am interested</a>
                                                </div>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class="project-item fivth-child mix start web">
                                        <ul class="event-item start">
                                            <li>
                                                <div class="date">
                                                    <span>16<br>Mar</span>
                                                </div>
                                            </li>
                                            <li>
                                                <h4>hh craft beer copper</h4>
                                                <div class="app">
                                                    <span>App Start Up, Web Conferences</span>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="time">
                                                    <span>11:30 AM - 04:30 PM<br>Friday</span>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="white-button">
                                                    <a href="#">I am interested</a>
                                                </div>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <section class="services" id="services-section">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-3">
                    <div class="flip-container" ontouchstart="this.classList.toggle('hover');">
                        <div class="flipper first-service">
                            <div class="front">

                                <div class="icon">
                                    <img src="img/heart-icon.png" alt="">

                                </div>
                                <h4>SIMIT</h4>
                                <a href="https://simitporcedula.com.co/"</a>
                            </div>
                            <div class="back">
                                <p>Consulta comparendos</p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="flip-container" ontouchstart="this.classList.toggle('hover');">
                        <div class="flipper second-service">
                            <div class="front">

                                <div class="icon">
                                    <img src="img/LogoRunt.png" alt="">

                                </div>
                                <h4>Acceda a la información más relevante para usted</h4>
                                <a href="https://www.runt.com.co/"</a>
                            </div>
                            <div class="back">
                              <p>Consulta por placa <br>
                              Consulta Runt <br>
                              Consulta de Infracciones <br>
                              Contulta Tipo de documento <br>
                              Historico Vehicular
                            </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="flip-container" ontouchstart="this.classList.toggle('hover');">
                        <div class="flipper first-service">
                            <div class="front">
                                <div class="icon">
                                    <img src="img/descarga.jpg" alt="">
                                </div>
                                <h4>Sistema Integrado de Información de la Proteccion Social</h4>
                                <a href="https://www.sispro.gov.co"</a>
                            </div>
                            <div class="back">
                                <p></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="flip-container" ontouchstart="this.classList.toggle('hover');">
                        <div class="flipper second-service">
                            <div class="front">
                                <div class="icon">
                                    <img src="img/descarga.png" alt="">
                                </div>
                                <h4>Consule AQUI su puesto de votación </h4>
                                <a href="https://www.registraduria.gov.co/"</a>
                            </div>
                            <div class="back">
                              <p>Cedula de Ciudadania <br>
                              Tramites <br>
                              Estado del Tramite <br>
                              Regsitro Civil <br>
                              Censo Nacional Electoral <br>
                              Agendamiento de Citas
                            </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="services" id="services-section">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-3">
                    <div class="flip-container" ontouchstart="this.classList.toggle('hover');">
                        <div class="flipper second-service">
                            <div class="front">
                                <div class="icon">
                                    <img src="img/poli.png" alt="">
                                </div>
                                <h4>Consulta AQUI Antecedentes Judiciales</h4>
                                  <a href="https://antecedentes.policia.gov.co:7005/WebJudicial/"</a>

                            </div>
                            <div class="back">
                                <p></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="flip-container" ontouchstart="this.classList.toggle('hover');">
                        <div class="flipper first-service">
                            <div class="front">
                                <div class="icon">
                                    <img src="img/Logo_IDPAC-01.png" alt="">
                                </div>
                                <h4>Instituto Distrital de Participacion Ciudadana</h4>
                                <a href="https://www.participacionbogota.gov.co/votoelectronicojac"</a>

                            </div>
                            <div class="back">
                              <p>Certificado de Existencia <br>
                              Voto Electrónico  <br>
                              Consejor Locale de la Bicicleta <br>
                              Escuela de Participacion

                            </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="flip-container" ontouchstart="this.classList.toggle('hover');">
                        <div class="flipper third-service">
                            <div class="front">
                                <div class="icon">
                                    <img src="img/procu.png" alt="">
                                </div>
                                <h4>Procuraduría General de la Nación</h4>
                                <a href="https://www.procuraduria.gov.co/portal/"</a>
                            </div>
                            <div class="back">
                              <p>Certificado de Antecedentes <br>
                              Consultar Antecedentes  <br>
                              Antención al Ciudadano  <br>
                              Pqrsdf
                            </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="flip-container" ontouchstart="this.classList.toggle('hover');">
                        <div class="flipper second-service">
                            <div class="front">
                                <div class="icon">
                                    <img src="img/secre.png" alt="">
                                </div>
                                <h4>Secretaria Distrital de Gobierno</h4>
                                <a href="http://www.gobiernobogota.gov.co/"</a>
                            </div>
                            <div class="back">
                              <p>Intranet <br>
                              Propiedad Horizontal   <br>
                              Antención al Ciudadano  <br>
                              Solicitud Certificado de Residencia <br>
                              Tramites y SErvicios
                            </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <footer>
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <p> &copy;
                  UniversidadPilotoDeColombia</p>
                </div>
            </div>
        </div>
    </footer>

    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.2.min.js"><\/script>')</script>

    <script src="js/vendor/bootstrap.min.js"></script>

    <script src="js/datepicker.js"></script>
    <script src="js/plugins.js"></script>
    <script src="js/main.js"></script>

    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js" type="text/javascript"></script>
    <script type="text/javascript">
    $(document).ready(function()
	{
        // navigation click actions
        $('.scroll-link').on('click', function(event){
            event.preventDefault();
            var sectionID = $(this).attr("data-id");
            scrollToID('#' + sectionID, 750);
        });
        // scroll to top action
        $('.scroll-top').on('click', function(event) {
            event.preventDefault();
            $('html, body').animate({scrollTop:0}, 'slow');
        });
        // mobile nav toggle
        $('#nav-toggle').on('click', function (event) {
            event.preventDefault();
            $('#main-nav').toggleClass("open");
        });
    });
    // scroll function
    function scrollToID(id, speed){
        var offSet = 0;
        var targetOffset = $(id).offset().top - offSet;
        var mainNav = $('#main-nav');
        $('html,body').animate({scrollTop:targetOffset}, speed);
        if (mainNav.hasClass("open")) {
            mainNav.css("height", "1px").removeClass("in").addClass("collapse");
            mainNav.removeClass("open");
        }
    }
    if (typeof console === "undefined") {
        console = {
            log: function() { }
        };
    }
    </script>
</body>
</html>

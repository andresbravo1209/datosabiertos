<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">

    <!-- Navbar -->
    <nav class="main-header navbar navbar-expand navbar-white navbar-light">
        <!-- Left navbar links -->
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
            </li>
            <li class="nav-item d-none d-sm-inline-block">
                <a href="/" class="nav-link">Home</a>
            </li>
        </ul>
    </nav>
    <!-- /.navbar -->

    <!-- Main Sidebar Container -->
    <aside class="main-sidebar sidebar-dark-primary elevation-4">
        <!-- Brand Logo -->
        <a href="/" class="brand-link">
            <img src="dist/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
                 style="opacity: .8">
            <span class="brand-text font-weight-light">Servicios Inteligentes</span>
        </a>
        <!-- Sidebar -->
        <div class="sidebar">
            <!-- Sidebar user panel (optional) -->
            <div class="user-panel mt-3 pb-3 mb-3 d-flex">
                <div class="image">
                    <img src="dist/img/user2-160x160.jpg" class="img-circle elevation-2" alt="User Image">
                </div>
                <div class="info">
                    <a href="#" class="d-block">Bienvenido!</a>
                </div>
            </div>
            <!-- Sidebar Menu -->
            <nav class="mt-2">
                <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                    <li class="nav-header">SERVICIOS</li>
                    <li class="nav-item">
                        <a href="/salud" class="nav-link">
                            <i class="nav-icon far fa-hospital"></i>
                            <p>
                                Salud
                            </p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="/vivienda" class="nav-link">
                            <i class="nav-icon far fa-hospital"></i>
                            <p>
                                Vivienda
                            </p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="/movilidad" class="nav-link">
                            <i class="nav-icon far fa-motorcycle"></i>
                            <p>
                                Movilidad
                            </p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="/results-icfes" class="nav-link">
                            <i class="nav-icon far fa-user"></i>
                            <p>
                                Educación
                            </p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="/contratacion" class="nav-link">
                            <i class="nav-icon far fa-copy"></i>
                            <p>
                                Contratación
                            </p>
                        </a>
                    </li>

                </ul>
            </nav>
        </div>
    </aside>

    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">CUADRANTE POLICIAL</h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active">Dashboard</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                {{--INICIO DE SECCION--}}
                {{--FIN DE SECCION--}}
            </div>
        </section>
        <section class="col-lg-5 connectedSortable">
        </section>
    </div>
</div>


<footer class="main-footer">
    <strong>Smart cities &copy; 2019 <a href="http://adminlte.io">UPC</a>.</strong>
    All rights reserved.
    <div class="float-right d-none d-sm-inline-block">
        <b>Version</b> 0.0.1
    </div>
</footer>
<aside class="control-sidebar control-sidebar-dark">
</aside>
<!-- jQuery -->
<script src="{{ asset('plugins/jquery/jquery.min.js') }}"></script>
<!-- jQuery UI 1.11.4 -->
<script src="{{ asset('plugins/jquery-ui/jquery-ui.min.js') }}"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
    $.widget.bridge('uibutton', $.ui.button)
</script>
<!-- Bootstrap 4 -->
<script src="{{ asset('plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('dist/js/adminlte.js') }}"></script>

</body>
